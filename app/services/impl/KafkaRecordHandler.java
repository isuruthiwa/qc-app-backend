package services.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import org.apache.log4j.Logger;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import play.libs.Json;
import play.mvc.BodyParser;
import services.dtos.ItemQuality;

import java.io.IOException;

public class KafkaRecordHandler implements Runnable {

    ConsumerRecord<String, String> record;

    private static Logger log;

    private final String INDEX_NAME = "quality-check";
    private final String DOC_TYPE = "itemQuality";

    private final RestClientBuilder clientBuilder = RestClient.builder(new HttpHost("localhost", 9200));

    private final RestHighLevelClient highLevelClient = new RestHighLevelClient(
            clientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder)
    );

    public IndexResponse addToElasticSearch(ItemQuality itemQuality) throws IOException {
        IndexRequest request = new IndexRequest(INDEX_NAME).type(DOC_TYPE)
                .source(Json.stringify(Json.toJson(itemQuality)), XContentType.JSON);
        return highLevelClient.index(request, RequestOptions.DEFAULT);
    }

    @Override
    public void run() {
        try {
            convertJsonAndSubmitToES(record.value());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public KafkaRecordHandler(ConsumerRecord<String, String> record) {
        this.record=record;
        log = Logger.getLogger(KafkaRecordHandler.class.getName());
    }

    @BodyParser.Of(BodyParser.Json.class)
    public void convertJsonAndSubmitToES(String record) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode json = objectMapper.readTree(record);
        ItemQuality itemQuality = Json.fromJson(json, ItemQuality.class);
        if(itemQuality.time!=null) {
            IndexResponse response = addToElasticSearch(itemQuality);
            log.info(response);
            log.info(itemQuality.toString());
        }
        else
            log.error("Invalid Time in record : "+record);
    }
}