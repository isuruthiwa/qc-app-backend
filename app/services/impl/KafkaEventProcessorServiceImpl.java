package services.impl;

import com.google.inject.Singleton;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.Logger;
import services.KafkaEventProcessorService;

import javax.inject.Inject;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Singleton
public class KafkaEventProcessorServiceImpl implements KafkaEventProcessorService {

    final private String TOPIC="QualityCheck";
    KafkaConsumer<String,String> kafkaConsumer;
    Thread kafkaConsumerThread;

    KafkaProducer<String, String> kafkaProducer;

    ThreadPoolExecutor executor;

    private static Logger log;

    private void createProducer(){
        Properties properties = new Properties();
        properties.put("bootstrap.servers","localhost:9092");
        properties.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");

        kafkaProducer=new KafkaProducer<>(properties);
    }

    private void createConsumer(){
        Properties properties = new Properties();
        properties.put("bootstrap.servers","localhost:9092");
        properties.put("group.id","quality-check-group");
//        properties.put("enable.auto.commit","true");
        properties.put("auto.offset.reset","earliest");
//        properties.put("auto.commit.interval.ms","1000");
        properties.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");

        kafkaConsumer=new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(Collections.singletonList(this.TOPIC));
    }

    @Override
    public void addToTopic(String messageKey, String message) {
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(this.TOPIC, messageKey, message);
        kafkaProducer.send(producerRecord);
        log.info("Added to kafka topic: "+ TOPIC);
    }

    @Inject
    public KafkaEventProcessorServiceImpl(){
        log = Logger.getLogger(KafkaEventProcessorService.class.getName());
        createProducer();
        createConsumer();
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        Runnable kafkaConsumerRunnable = this::consumerInit;
        kafkaConsumerThread = new Thread(kafkaConsumerRunnable);
        kafkaConsumerThread.start();
    }

    @Override
    public void consumerInit(){
        log.info("Kafka Consumer thread started");
        while (true){
            ConsumerRecords<String, String> records;
            records = kafkaConsumer.poll(50);
            for(ConsumerRecord<String, String> record: records){
                log.info("Kafka event");
                executor.execute(new KafkaRecordHandler(record));
            }

        }
    }
}