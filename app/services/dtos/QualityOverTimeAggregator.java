package services.dtos;

import java.util.List;

public class QualityOverTimeAggregator {
    String interval;
    long intervalCount;
    List<QualityAggregator> qualityList;

    public QualityOverTimeAggregator(String interval, long intervalCount, List<QualityAggregator> qualityList) {
        this.interval = interval;
        this.intervalCount = intervalCount;
        this.qualityList = qualityList;
    }

    @Override
    public String toString() {
        return "QualityOverTimeAggregator{" +
                "key='" + interval + '\'' +
                ", count=" + intervalCount +
                ", qualityAggregatorList=" + qualityList +
                '}';
    }
}
