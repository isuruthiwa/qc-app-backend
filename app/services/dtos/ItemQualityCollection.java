package services.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Date;

public class ItemQualityCollection {
    public String item;
    @JsonDeserialize(using = DateHandler.class)
    public Date time;
    public long passCount;
    public long failCount;

    public ItemQualityCollection(String item, Date time, long passCount, long failCount) {
        this.item = item;
        this.time = time;
        this.passCount = passCount;
        this.failCount = failCount;
    }

    public ItemQualityCollection() {

    }

    @Override
    public String toString() {
        return "ItemQualityCollection{" +
                "item='" + item + '\'' +
                ", time=" + time +
                ", passCount=" + passCount +
                ", failCount=" + failCount +
                '}';
    }
}
