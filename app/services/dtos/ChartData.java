package services.dtos;

import java.util.List;

public class ChartData {
    long totalData;
    long totalDefects;
    double defectRatio;
    List<IntervalDefects> defects;

    public ChartData(long totalData, long totalDefects, double defectRatio, List<IntervalDefects> defects) {
        this.totalData = totalData;
        this.totalDefects = totalDefects;
        this.defectRatio = defectRatio;
        this.defects = defects;
    }

    public ChartData(){

    }
}
