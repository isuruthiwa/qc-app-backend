package services.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class IntervalDefects {
    private static final String LANKA_TIME_ZONE="Asia/Colombo";

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = LANKA_TIME_ZONE)
    Date time;
    Long defectCount;

    public IntervalDefects(Date time, Long defectCount) {

        this.time = time;
        this.defectCount = defectCount;
    }

    public IntervalDefects() {
    }
}
