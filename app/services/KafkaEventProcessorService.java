package services;

import com.google.inject.ImplementedBy;
import services.impl.KafkaEventProcessorServiceImpl;

@ImplementedBy(KafkaEventProcessorServiceImpl.class)
public interface KafkaEventProcessorService {
    void consumerInit();
    void addToTopic(String messageKey, String message);
}
