package services;

import com.google.inject.ImplementedBy;
import org.elasticsearch.action.index.IndexResponse;
import services.dtos.ChartData;
import services.dtos.ItemQuality;
import services.dtos.QualityOverTimeAggregator;
import services.impl.DataServiceImpl;

import java.io.IOException;
import java.util.List;

@ImplementedBy(DataServiceImpl.class)
public interface DataService {
    IndexResponse addNewEvent(ItemQuality itemQuality) throws IOException;
    List<QualityOverTimeAggregator> getLastTenRecords() throws IOException;
    ChartData getChartDefects() throws IOException;
}
